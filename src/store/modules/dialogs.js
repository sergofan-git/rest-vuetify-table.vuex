const state = {
  userDialog: false,
  confirmDialog: false,
  newUser: false
};

const getters = {
  userDialog: state => state.userDialog,
  confirmDialog: state => state.confirmDialog,
  newUser: state => state.newUser
};

const actions = {
  setUserDialog({ commit }, opened) {
    commit("SET_USER_DIALOG", opened);
  },
  setConfirmDialog({ commit }, opened) {
    commit("SET_CONFIRM_DIALOG", opened);
  },
  setNewUser({ commit }, value) {
    commit("SET_NEW_USER", value);
  }
};

const mutations = {
  SET_USER_DIALOG(state, opened) {
    state.userDialog = opened;
  },
  SET_CONFIRM_DIALOG(state, opened) {
    state.confirmDialog = opened;
  },
  SET_NEW_USER(state, value) {
    state.newUser = value;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
