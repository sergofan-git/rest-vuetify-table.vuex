import { getData } from "../../async-util";

const state = {
  root: true,
  users: [],
  props: "email,name",
  pagination: {
    descending: false,
    page: 1,
    rowsPerPage: 5,
    sortBy: "name",
    totalItems: 0
  },
  search: "",

  // pageSize: 5,
  // currentPage: 1,
  loading: false
};

const getters = {
  users: state => state.users,
  pagination: state => state.pagination,
  props: state => state.props,
  search: state => state.search
};

const actions = {
  loadUsers({ commit, state }) {
    commit("SET_LOADING", true);
    getData(state.props, state.pagination, state.search).then(res => {
      if (res) {
        commit("SET_USERS", res.data);
        commit("SET_TOTAL_ITEMS", Number(res.headers["x-total-count"]));
      }
      commit("SET_LOADING", false);
    });
  }
};

const mutations = {
  SET_USERS(state, users) {
    state.users = users;
  },
  SET_TOTAL_ITEMS(state, totalItems) {
    state.pagination.totalItems = totalItems;
  },
  SET_PAGINATION(state, pagination) {
    state.pagination = pagination;
  },
  SET_PAGE(state, page) {
    state.pagination.page = page;
  },
  SET_PROPS(state, props) {
    state.props = props;
  },
  SET_SEARCH(state, search) {
    state.search = search;
  },
  SET_LOADING(state, loading) {
    state.loading = loading;
  },
  REMOVE_USER(state, user) {
    let users = state.users;
    let idx = users.findIndex(x => x.id == user.id);
    users.splice(idx, 1);
  },
  ADD_USER(state, user) {
    state.users.push(user);
  },
  PUT_USER(state, user) {
    let users = state.users;
    let foundIndex = users.findIndex(x => x.id == user.id);
    Object.assign(users[foundIndex], user);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
