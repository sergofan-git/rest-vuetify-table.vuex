import axios from "axios";

const url = "http://localhost:3000/users";
// const url =
//   "https://api.backendless.com/53FB6874-BEE0-9546-FFCA-1F3DEE56BE00/73E38E38-C5C9-45B2-FFB5-D05A6E16A600/data/Users";

async function getData(props, pagination, search) {
  try {
    const order = function() {
      switch (pagination.descending) {
        case true:
          return "DESC";
        case false:
          return "ASC";
        default:
          return null;
      }
    };
    const res = await axios(url, {
      params: {
        q: search,
        props: props,
        _limit: pagination.rowsPerPage,
        _page: pagination.page,
        _sort: pagination.sortBy,
        _order: order()
      }
    });
    return res;
  } catch (e) {
    console.error(e);
  }
}

async function addData(user) {
  try {
    const res = await axios.post(url, user);
    console.log("added user", res.data);
    return res.data;
  } catch (e) {
    console.error(e);
  }
}

async function putData(user) {
  try {
    const res = await axios.put(url + "/" + user.id, user);
    console.log("updated user", res.data);
    return res.data;
  } catch (e) {
    console.error(e);
  }
}

async function deleteData(user) {
  try {
    await axios.delete(url + "/" + user.id);
    console.log("removed user", Object.assign({}, user));
    return user;
  } catch (e) {
    console.error(e);
  }
}

export { getData, addData, putData, deleteData };
