import { addData, putData, deleteData } from "../../async-util";

const state = {
  user: {}
};

const getters = {
  user: state => state.user
};

const actions = {
  setUser({ commit }, user) {
    commit("SET_USER", user);
  },
  addUser({ commit }, user) {
    const obj = Object.assign({}, user);
    addData(obj).then(data => {
      commit("users/ADD_USER", data, { root: true });
    });
  },
  removeUser({ commit }, user) {
    deleteData(user).then(_ => {
      commit("users/REMOVE_USER", user, { root: true });
    });
  },
  clearUser({ commit }) {
    commit("CLEAR_USER");
  },
  putUser({ commit }, user) {
    putData(user).then(data => {
      commit("users/PUT_USER", data, { root: true });
    });
  }
};

const mutations = {
  SET_USER(state, user) {
    state.user = user;
  },
  CLEAR_USER(state) {
    state.user = "";
  },
  UPDATE_USER(state, user) {
    Object.assign(state.user, user);
  },
  PUT_USER(state, user) {
    Object.assign(state.user, user);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
