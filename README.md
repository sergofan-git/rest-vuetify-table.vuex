# rest-vuetify-table.vuex

> A Vue.js project

[comment]: # "[Open demo]"
[comment]: # '[open demo]: https://sergofan-git.gitlab.io/rest-vuetify-table.vuex "Open demo"'

## Build Setup

```bash
# install dependencies
yarn

# start server
yarn server

# serve with hot reload at localhost:8080
yarn dev

# build for production with minification
yarn build

# build for production and view the bundle analyzer report
yarn build --report

# run unit tests
yarn unit

# run all tests
yarn test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
